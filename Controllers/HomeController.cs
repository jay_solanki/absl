﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ABSL.Models;

namespace ABSL.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            Club club = LoadED();

            ViewData["Club"] = LoadED();
            ViewData["Clubs"] = LoadAllClubs();

            ViewData["Title"] = DateTime.Now.ToString("HH:mm / ") + club.Name;

            return View();
        }

        [HttpGet("/{club_name}")]
        [HttpGet("Home/{club_name}")]
        public IActionResult Index(string club_name)
        {
            Club club = null;

            if (club_name.Equals("Eastern-Dragons"))
            {
                club = LoadED();
                ViewData["Club"] = LoadED();
            }
            else if (club_name.Equals("AC-Cougars"))
            {
                club = LoadAC();
                ViewData["Club"] = LoadAC();
            }
            else
            {
                club = LoadED();
                ViewData["Club"] = LoadED();
            }

            ViewData["Clubs"] = LoadAllClubs();
            ViewData["Title"] = DateTime.Now.ToString("HH:mm / ") + club.Name;

            return View();
        }

        public IActionResult About(string valv)
        {
            ViewData["Club"] = LoadED();

            ViewData["Message"] = valv;

            return View("About");
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //////////

        private List<Pages> LoadAllClubs()
        {
            List<Pages> clubs = new List<Pages>();
            clubs.Add(new Pages("Eastern Dragons", "/Eastern-Dragons", false, null));
            clubs.Add(new Pages("Auckland Central Courgars", "/AC-Cougars", false, null));

            return clubs;
        }

        private Club LoadED()
        {
            StyleModel style = new StyleModel()
            {
                LogoURL = "https://static.wixstatic.com/media/2db6b8_04f6bb6e852445b2ba1338ad7a7117ad~mv2.jpg/v1/fill/w_146,h_108,al_c,q_80,usm_0.66_1.00_0.01/Eastern-Dragons-Logo-300x225.webp",
                Color1 = "#FB9233",
                Color2 = "#EA6E21",
                Color3 = "#e5600d",
                BG1 = "https://static.wixstatic.com/media/2db6b8_9ba0395995bd46df86245c510b121a8c~mv2_d_4032_3024_s_4_2.jpg",
                BG2 = "https://static.wixstatic.com/media/2db6b8_3e25484dc9aa435681cf47f65f751a85~mv2.jpg"
            };

            List<Slideshow> slideshow = new List<Slideshow>();
            List<Pages> pages = new List<Pages>();

            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/2db6b8_3f615f2c48ae4d87a957212e9ca48eda~mv2_d_3024_4032_s_4_2.jpg/v1/fill/w_408,h_253,al_c,q_80,usm_0.66_1.00_0.01/2db6b8_3f615f2c48ae4d87a957212e9ca48eda~mv2_d_3024_4032_s_4_2.jpg", "EASTERN BASKETBALL CLUB 1"));
            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/2db6b8_b88f6e2bfb924e61a3b572e769c9b4ba~mv2_d_4032_3024_s_4_2.jpg/v1/fill/w_408,h_253,al_c,q_80,usm_0.66_1.00_0.01/2db6b8_b88f6e2bfb924e61a3b572e769c9b4ba~mv2_d_4032_3024_s_4_2.jpg", "EASTERN BASKETBALL CLUB 2"));
            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/2db6b8_31db466d31354833af1ea4f6cd6216f5~mv2_d_4032_3024_s_4_2.jpg/v1/fill/w_408,h_253,al_c,q_80,usm_0.66_1.00_0.01/2db6b8_31db466d31354833af1ea4f6cd6216f5~mv2_d_4032_3024_s_4_2.jpg", "EASTERN BASKETBALL CLUB 3"));

            List<Pages> events = new List<Pages>();
            events.Add(new Pages("Special Events", "/Home/Special-Events", false, null));
            events.Add(new Pages("Upcoming Events", "/Home/Upcoming-Events", false, null));

            pages.Add(new Pages("Programmes", "/Home/Programmes", false, null));
            pages.Add(new Pages("Events", "/Home/Events", true, events));
            pages.Add(new Pages("News", "/Home/News", false, null));
            pages.Add(new Pages("About Us", "/Home/About", false, null));
            pages.Add(new Pages("Contact", "/Home/Contact", false, null));

            return new Club("Eastern Dragons", "Welcome To The Eastern Basketball Club", "The Home of Basketball in East Auckland", style, slideshow, pages);
        }

        private Club LoadAC()
        {
            StyleModel style = new StyleModel()
            {
                LogoURL = "https://static.wixstatic.com/media/c959e7_84ea32988d2440909886c7fb4041dd74~mv2_d_2398_2399_s_2.png/v1/fill/w_100,h_100,al_c,q_80,usm_0.66_1.00_0.01/c959e7_84ea32988d2440909886c7fb4041dd74~mv2_d_2398_2399_s_2.webp",
                Color1 = "#5e68e0",
                Color2 = "#454C9F",
                Color3 = "#454C9F",
                BG1 = "https://static.wixstatic.com/media/c959e7_29e546890f8e44bbb7e01b10a937b70a~mv2_d_1536_2048_s_2.jpg",
                BG2 = "https://static.wixstatic.com/media/c959e7_25dcd9dc57384915b39be4bf2c925a52~mv2_d_2048_1820_s_2.jpg"
            };

            List<Slideshow> slideshow = new List<Slideshow>();
            List<Pages> pages = new List<Pages>();

            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/c959e7_d8d6f8e19db54ccaa0a319f7d3e6fade~mv2.jpg/v1/fill/w_686,h_457,fp_0.50_0.50,q_90/c959e7_d8d6f8e19db54ccaa0a319f7d3e6fade~mv2.webp?retry=1", "AUCKLAND CENTRAL COUGARS"));
            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/c959e7_36b00294b0fd41999985c3ea07b84ede~mv2.jpg/v1/fill/w_686,h_457,fp_0.50_0.50,q_90/c959e7_36b00294b0fd41999985c3ea07b84ede~mv2.webp?retry=1", "AUCKLAND CENTRAL COUGARS"));
            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/c959e7_885f5b209264412889f1b7f498e28ded~mv2.jpg/v1/fill/w_686,h_457,fp_0.50_0.50,q_90/c959e7_885f5b209264412889f1b7f498e28ded~mv2.webp?retry=1", "AUCKLAND CENTRAL COUGARS"));
            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/c959e7_0c3e71d8ba5c4d1b839c987faa34a190~mv2.jpg/v1/fill/w_686,h_457,fp_0.50_0.50,q_90/c959e7_0c3e71d8ba5c4d1b839c987faa34a190~mv2.webp?retry=1", "AUCKLAND CENTRAL COUGARS"));
            slideshow.Add(new Slideshow("https://static.wixstatic.com/media/c959e7_af92694948994a76b2b6bb5c8278397c~mv2.jpg/v1/fill/w_686,h_457,fp_0.50_0.50,q_90/c959e7_af92694948994a76b2b6bb5c8278397c~mv2.webp?retry=1", "AUCKLAND CENTRAL COUGARS"));

            List<Pages> about = new List<Pages>();
            about.Add(new Pages("About Us", "/Home/About", false, null));
            about.Add(new Pages("About Leagues", "/Home/About-Leagues", false, null));
            about.Add(new Pages("FAQ", "/Home/FAQ", false, null));
            about.Add(new Pages("Gallery", "/Home/Gallery", false, null));

            pages.Add(new Pages("About", "/Home/About", true, about));
            pages.Add(new Pages("Services", "/Home/Services", false, null));
            pages.Add(new Pages("Competitions", "/Home/Competitions", false, null));
            pages.Add(new Pages("Programmes", "/Home/Programmes", false, null));
            pages.Add(new Pages("Contact", "/Home/Contact", false, null));

            return new Club("Auckland Central Cougars", "One Club One Team One Family", "Welcome To Auckland Central Cougars Basketball Club", style, slideshow, pages);
        }
    }
}
