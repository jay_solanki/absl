﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSL.Models
{
    public class Slideshow
    {
        public string Image { get; set; }
        public string Tagline { get; set; }

        public Slideshow(string image, string tagline)
        {
            Image = image;
            Tagline = tagline;
        }
    }
}
