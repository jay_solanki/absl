﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSL.Models
{
    public class Club
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Tagline { get; set; }

        public StyleModel style { get; set; }
        public List<Pages> pages { get; set; }
        public List<Slideshow> slideshow { get; set; }

        public Club(string name, string title, string tagline, StyleModel style, List<Slideshow> slideshow, List<Pages> pages)
        {
            Name = name;
            Title = title;
            Tagline = tagline;
            this.style = style;
            this.slideshow = slideshow;
            this.pages = pages;
        }
    }
}
