﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSL.Models
{
    public class StyleModel
    {
        public string LogoURL { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public string Color3 { get; set; }
        public string BG1 { get; set; }
        public string BG2 { get; set; }
    }
}
