﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSL.Models
{
    public class Pages
    {
        public string Title { get; set; }
        public string URL { get; set; }
        public bool isMultiple { get; set; }
        public List<Pages> others { get; set; }

        public Pages(string title, string uRL, bool isMultiple, List<Pages> others)
        {
            Title = title;
            URL = uRL;
            this.isMultiple = isMultiple;
            this.others = others;
        }
    }
}
